#!/usr/bin/python3
"""
Create chastity report charts from lockup data

Copyright 2022 nicki.pet

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import argparse
from collections import namedtuple
import csv
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import os
from pathlib import Path
from statistics import median

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


SUBJECTS_DIR = Path("subjects")
REPORTS = {
    "lockup-overview",
}

Lockup = namedtuple("Lockup", ["start", "end"])
Report = namedtuple("Report", ["name", "lockups"])


def start_of_months(since, until):
    months = []
    labels = []
    current_m = since.month + 1
    current_y = since.year
    if current_m > 12:
        current_m = 1
        current_y += 1
    current = datetime(current_y, current_m, 1)

    while current < until:
        months.append(current)
        labels.append(f"{current:%b}")
        current += relativedelta(months=1)

    return months, labels


def start_of_weeks(since, until):
    weeks = []
    current = since

    while current < until:
        weeks.append(current)
        current += relativedelta(weeks=1)

    return weeks


def get_lockups(report):
    return [lockup.end - lockup.start for lockup in report.lockups]


def get_cravings(report):
    assert len(report.lockups) >= 2
    return [
        report.lockups[i + 1].start - report.lockups[i].end
        for i in range(len(report.lockups) - 1)
    ]


def create_lockup_overview(subject, report):
    fig = plt.figure(figsize=(7, 4))
    ax = fig.add_subplot()

    ax.set(
        xlim=(report.lockups[0].start, report.lockups[-1].end),
        ylim=(0, 3.5),
    )
    for lockup in report.lockups:
        ax.add_patch(
            Rectangle(
                (lockup.start, 0), lockup.end - lockup.start, 1, color="xkcd:pink"
            )
        )

    xticks_major, xlabels = start_of_months(
        report.lockups[0].start, report.lockups[-1].end
    )
    xticks_minor = start_of_weeks(report.lockups[0].start, report.lockups[-1].end)
    ax.set_xticks(xticks_minor, minor=True)
    ax.set_xticks(xticks_major, xlabels, rotation=40, color="xkcd:plum")

    plt.yticks([0, 1], ["Craving", "LOCKED"], color="xkcd:pink")

    plt.title(f"Chastity Sessions\n@{subject}")
    plt.grid(axis="x", color="xkcd:plum", alpha=0.5, linestyle="--")
    plt.grid(axis="x", which="minor", color="xkcd:magenta", alpha=0.3, linestyle=":")

    # boxes
    props = dict(boxstyle="round", facecolor="thistle", alpha=1)
    args = {
        "color": "xkcd:plum",
        "fontsize": 12,
        "fontname": "monospace",
        "transform": ax.transAxes,
        "bbox": props,
    }

    lockups = get_lockups(report)
    cravings = get_cravings(report)

    locked = sum(lockups, timedelta())
    craving = sum(cravings, timedelta())
    ratio = locked / (locked + craving)
    text = "\n".join(
        [
            "---- Overall ----",
            f"Sessions   {len(report.lockups): <4d}",
            f"Caged Time {ratio*100: >4.1f} %",
            "",
            f"LOCKED     {locked.days//7: >2d}w {locked.days%7: >1d}d",
            f"Craving    {craving.days//7: >2d}w {craving.days%7: >1d}d",
        ]
    )
    ax.text(0.05, 0.95, text, verticalalignment="top", **args)

    text = "\n".join(
        [
            "---- LOCKED ----",
            f"Median    {median(lockups).days//7: >2d}w {median(lockups).days%7: >1d}d",
            f"Longest   {max(lockups).days//7: >2d}w {max(lockups).days%7: >1d}d",
            f"Shortest  {min(lockups).days//7: >2d}w {min(lockups).days%7: >1d}d",
        ]
    )
    ax.text(
        0.95, 0.95, text, verticalalignment="top", horizontalalignment="right", **args
    )

    text = "\n".join(
        [
            "---- Craving ---",
            f"Median    {median(cravings).days//7: >2d}w {median(cravings).days%7: >1d}d",
            f"Strongest {min(cravings).days//7: >2d}w {min(cravings).days%7: >1d}d",
            f"Weakest   {max(cravings).days//7: >2d}w {max(cravings).days%7: >1d}d",
        ]
    )
    ax.text(
        0.95, 0.4, text, verticalalignment="bottom", horizontalalignment="right", **args
    )

    text = "https://gitlab.com/nicki.pet/chastity-report"
    plt.text(
        0.98,
        0.968,
        text,
        transform=fig.transFigure,
        color="dimgray",
        fontsize=7,
        verticalalignment="top",
        horizontalalignment="right",
    )

    filename = SUBJECTS_DIR / subject / "lockup-overview" / f"{report.name}.png"
    if not filename.parent.is_dir():
        os.mkdir(filename.parent)
    plt.savefig(filename, dpi=300)


def get_lockup_reports(subject):
    csv_dir = SUBJECTS_DIR / subject / "lockup-csv"
    assert csv_dir.is_dir()
    lockup_reports = []
    for csv_filename in csv_dir.glob("*.csv"):
        with open(csv_filename, encoding="utf-8") as csv_file:
            reader = csv.DictReader(csv_file)
            report = Report(f"{csv_filename.stem}", [])
            for row in reader:
                report.lockups.append(
                    Lockup(
                        datetime.fromisoformat(row["start_lockup"]),
                        datetime.fromisoformat(row["end_lockup"]),
                    )
                )
            lockup_reports.append(report)
    return lockup_reports


def main():
    parser = argparse.ArgumentParser(
        description="Create chastity report(s) from lockup data."
    )
    parser.add_argument(
        "-s",
        "--subject",
        required=True,
        type=str,
        help="experiment subject name; directory must exists in subjects/",
    )
    args = parser.parse_args()

    for report in get_lockup_reports(args.subject):
        create_lockup_overview(args.subject, report)


if __name__ == "__main__":
    main()
