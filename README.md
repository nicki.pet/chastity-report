Chastity Report
===============

Generate chastity charts from lockup data.

![Lockup Overview](subjects/nicki_pet/lockup-overview/2022-mar01-to-oct31.png)

Lockup Data Format
------------------

- CSV file with two datetimes - start of lockup, end of lockup
- see example in subjects/nicki_pet

License
-------

Distriubed under 3-Clause BSD License.
